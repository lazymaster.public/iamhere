//
//  NotificationService.swift
//  http
//
//  Created by LazyMaster.iMac on 2021/08/12.
//

import UserNotifications


class NotificationService: UNNotificationServiceExtension {
	let session: URLSession = URLSession.shared

	var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
	
	var sendDate: String {
		let date = Int64(Date().timeIntervalSince1970)
		
		return String(format: "%ld", date * 1000)
	}

	
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)

		if let bestAttemptContent = bestAttemptContent {
			if let _action = bestAttemptContent.userInfo["action"] as? String {
				// 위치 요청 받음
				if _action == "reqLoc" {
					if let _reqKey = bestAttemptContent.userInfo["reqKey"] as? String, let _type = bestAttemptContent.userInfo["type"] as? String {
						let token = MyInfoModel.shared.getToken() ?? ""
						let latitude = String(UtilLocation.shared.currentLocationString().latitude.prefix(6))
						let longitude = String(UtilLocation.shared.currentLocationString().longitude.prefix(6))

						bestAttemptContent.title = "위치 요청 받음! (\(token))"
						bestAttemptContent.subtitle = "\(latitude) : \(longitude)"

						guard FriendModel.shared.hasFriend(token: _reqKey) else {
							return
						}

						let url: URL = URL(string: "https://fcm.googleapis.com/fcm/send")!
						let paramsNoti = [
							"title": "위치 요청",
							"body": "위치를 받았습니다.",
							"sound": "default",
						]
						
						let paramsGPS = [
							"providerName": "gps",
							"isFakeGps": "false",
							"latitude": UtilLocation.shared.currentLocationString().latitude,
							"longitude": UtilLocation.shared.currentLocationString().longitude,
							"time": self.sendDate,
						]
						
						let paramsNetwork = [
							"providerName": "network",
							"isFakeGps": "false",
							"latitude": UtilLocation.shared.currentLocationString().latitude,
							"longitude": UtilLocation.shared.currentLocationString().longitude,
							"time": self.sendDate,
						]

						let paramsLocation: [String: Any] = [
							"pkg": "IamHere",
							"action": "resLoc",
							"type": "ios",
							"resKey": MyInfoModel.shared.getToken() ?? "",
							"gps": paramsGPS,
							"network": paramsNetwork,
							"resCode": "1000"
						]

						var params = [String: Any]()

						if _type == "ios" {
							params["notification"] = paramsNoti
						}
						
						params["to"] = _reqKey
						params["data"] = paramsLocation
						params["time_to_live"] = 5
						params["priority"] = "high"
						params["mutable_content"] = true

						self.post(url: url, body: params) {
							if let _httpResponse = $1 as? HTTPURLResponse {
								print("status [\(_httpResponse.statusCode)]")
							}

							if let _data = $0 {
								print("data [\(_data)]")
							}
							
							if let _error = $2 {
								print("error [\(_error)]")
							}
						}
						
						sleep(1)
					}
				} else if _action == "userRegOk" {
					if let _reqToken = bestAttemptContent.userInfo["reqKey"] as? String,
					   let _resToken = bestAttemptContent.userInfo["resKey"] as? String,
					   let _type = bestAttemptContent.userInfo["type"] as? String {
						if _reqToken == MyInfoModel.shared.getToken() {
							let _ = FriendModel.shared.add(token: _resToken, ios: _type == "ios")
						}
					}
				}
			}

            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
	
	func post(url: URL, body: [String: Any], completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
		var request: URLRequest = URLRequest(url: url)
		request.httpMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue("key=\(DefineShare.fcmAppKey)", forHTTPHeaderField: "Authorization")
		request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted)
		session.dataTask(with: request, completionHandler: completionHandler).resume()
	}
}

