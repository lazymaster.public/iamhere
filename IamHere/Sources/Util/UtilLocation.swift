//
//  UtilLocation.swift
//  http
//
//  Created by LazyMaster.iMac on 2021/08/19.
//

import UIKit
import MapKit


public class UtilLocation: NSObject {
	static let shared = UtilLocation()
	
	var locationManager = CLLocationManager()
	var completion: ((Bool) -> ())?

	
	func checkRequest() -> Bool {
		let authorizationStatus: CLAuthorizationStatus
		
		if #available(iOS 14, *) {
			authorizationStatus = locationManager.authorizationStatus
		} else {
			authorizationStatus = CLLocationManager.authorizationStatus()
		}
		
		
		switch authorizationStatus {
		case .notDetermined:
			return false
		default:
			return true
		}
	}

	func checkLocation() -> Bool {
		let authorizationStatus: CLAuthorizationStatus
		
		if #available(iOS 14, *) {
			authorizationStatus = locationManager.authorizationStatus
		} else {
			authorizationStatus = CLLocationManager.authorizationStatus()
		}

		switch authorizationStatus {
		case .restricted, .denied:
			return false
		default:
			return true
		}
	}

	func checkLocationAlways() -> Bool {
		let authorizationStatus: CLAuthorizationStatus
		
		if #available(iOS 14, *) {
			authorizationStatus = locationManager.authorizationStatus
		} else {
			authorizationStatus = CLLocationManager.authorizationStatus()
		}

		switch authorizationStatus {
		case .authorizedAlways:
			return true
		default:
			return false
		}
	}

	func request(location: @escaping ((Bool) -> ())) {
		self.completion = location
		
		locationManager.allowsBackgroundLocationUpdates = true
//		locationManager.pausesLocationUpdatesAutomatically = false

		self.locationManager.requestWhenInUseAuthorization()
		
		self.locationManager.delegate = self
	}

	func requestAlways(location: @escaping ((Bool) -> ())) {
		self.completion = location
		
		locationManager.allowsBackgroundLocationUpdates = true
//		locationManager.pausesLocationUpdatesAutomatically = false

		self.locationManager.requestAlwaysAuthorization()
		
		self.locationManager.delegate = self
	}
	
	func currentLocation() -> (latitude: Double, longitude: Double) {
		if let _location = self.locationManager.location {
			return (latitude: _location.coordinate.latitude, longitude: _location.coordinate.longitude)
		} else {
			return (latitude: 0, longitude: 0)
		}
	}
	
	func currentLocationString() -> (latitude: String, longitude: String) {
		if let _location = self.locationManager.location {
			let latitude = String(format: "%f", _location.coordinate.latitude)
			let longitude = String(format: "%f", _location.coordinate.longitude)
			
			return (latitude: latitude, longitude: longitude)
		} else {
			return (latitude: "-", longitude: "-")
		}
	}
}

extension UtilLocation: CLLocationManagerDelegate {
	public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		if status == .authorizedAlways || status == .authorizedWhenInUse {
			self.completion?(true)
		} else {
			self.completion?(false)
		}
	}
}

