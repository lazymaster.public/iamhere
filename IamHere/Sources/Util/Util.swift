//
//  Util.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/26.
//

import UIKit
import KakaoSDKLink
import SafariServices


class Util: NSObject {
	static var currentDate: String {
		let formatter = DateFormatter()

//		formatter.dateFormat = "yyyy/MM/dd hh:mm:ss"
		formatter.dateFormat = "HH:mm:ss"

		return formatter.string(from: Date())
	}
	
	static var sendDate: String {
		let date = Int64(Date().timeIntervalSince1970)
		
		return String(format: "%ld", date * 1000)
	}
	
	
	static func receiveDate(time: String?) -> String {
		guard let _time = time else { return Util.currentDate }
		guard let _date = TimeInterval(_time) else { return Util.currentDate }

		let formatter = DateFormatter()
		let date = Date(timeIntervalSince1970: _date / 1000)

		formatter.dateFormat = "HH:mm:ss"

		return formatter.string(from: date)
	}
	
	static func kakaoLink() {
		let templateId: Int64 = 15062
		let pushKey = MyInfoModel.shared.getToken() ?? ""

		let appParams = "\(DefineShare.KAKAO_PARAM_TYPE)=\(DefineShare.KAKAO_PARAM_IOS)&\(DefineShare.KAKAO_PARAM_ACTION)=\(DefineShare.KAKAO_PARAM_ACTION_SHARE_LOCATION_REQ)&\(DefineShare.KAKAO_PARAM_PUSHKEY)=\(pushKey)&\(DefineShare.KAKAO_PARAM_USER_NAME)="

		let templateArgs = ["sendParam": appParams]
		
		if LinkApi.isKakaoLinkAvailable() {
			LinkApi.shared.customLink(templateId: templateId, templateArgs: templateArgs) {(linkResult, error) in
				if let error = error {
					print(error)
				} else {
					print("scrapLink() success.")
					if let linkResult = linkResult {
						UIApplication.shared.open(linkResult.url, options: [:], completionHandler: nil)
					}
				}
			}
		}
	}
	
	static func goAppSetting(completion: @escaping (() -> ())) {
		if let _url = URL(string: UIApplication.openSettingsURLString) {
			if #available(iOS 10, *) {
				UIApplication.shared.open(_url, options: [:]) { (isOK) in
					if isOK {
						completion()
					}
				}
			} else {
				UIApplication.shared.openURL(_url)
				completion()
			}
		}
	}
}

