//
//  Extension.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/26.
//

import UIKit


extension UIView {
	func setShadow(color: UIColor, alpha: CGFloat, radius: CGFloat, x: Int, y: Int) {
		self.layer.shadowColor = color.cgColor
		self.layer.shadowRadius = radius
		self.layer.shadowOpacity = Float(alpha)
		self.layer.shadowOffset = CGSize(width: x, height: y)
	}
}

extension NSObject {
	func topViewController(viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
		guard let controller = viewController else {
			return nil
		}
		
		if let navigationController = controller as? UINavigationController {
			return self.topViewController(viewController: navigationController.visibleViewController)
		}
		
		if let tabController = controller as? UITabBarController {
			if let selected = tabController.selectedViewController {
				return self.topViewController(viewController: selected)
			}
		}
		
		if let presented = controller.presentedViewController {
			return self.topViewController(viewController: presented)
		}
		
		return controller
	}
	
	func popToRoot() {
		guard let _viewController = self.topViewController() else {
			return
		}
		
		guard let _navagation = _viewController.navigationController else {
			return
		}
		
		_navagation.popToRootViewController(animated: false)
	}
}

