//
//  SendModel.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/24.
//

import UIKit
import RxSwift
import Moya


class SendModel: NetworkBase, Codable {
	static func request(ios: Bool, friendToken: String) -> Observable<SendModel> {
		return self.provider.rx.request(.send(ios: ios, friendToken: friendToken))
			.retry(3)
			.do(onSuccess: {
				do {
					if let _json = try $0.mapJSON() as? [String: Any] {
						print("------------- ok")
					}
				} catch {
				  print("------------- error")
				}
			})
			.map(SendModel.self)
			.asObservable()
	}
}

