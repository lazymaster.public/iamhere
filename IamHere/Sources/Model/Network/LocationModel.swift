//
//  LocationModel.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/25.
//

import UIKit
import RxSwift
import Moya


class LocationModel: NetworkBase, Codable {
	static func request(ios: Bool, friendToken: String) -> Observable<LocationModel> {
		return self.provider.rx.request(.location(ios: ios, friendToken: friendToken))
			.retry(3)
			.do(onSuccess: {
				do {
					if let _json = try $0.mapJSON() as? [String: Any] {
						print("------------- ok")
					}
				} catch {
				  print("------------- error")
				}
			})
			.map(LocationModel.self)
			.asObservable()
	}
}

