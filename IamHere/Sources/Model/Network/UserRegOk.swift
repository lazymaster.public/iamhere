//
//  UserRegOk.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/03.
//

import UIKit
import RxSwift
import Moya


class UserRegOk: NetworkBase, Codable {
	static func request(ios: Bool, friendToken: String) -> Observable<UserRegOk> {
		return self.provider.rx.request(.userRegOk(ios: ios, friendToken: friendToken))
			.retry(3)
			.do(onSuccess: {
				do {
					if let _json = try $0.mapJSON() as? [String: Any] {
						print("------------- ok")
					}
				} catch {
				  print("------------- error")
				}
			})
			.map(UserRegOk.self)
			.asObservable()
	}
}

