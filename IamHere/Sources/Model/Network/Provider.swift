//
//  Network.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/24.
//

import Moya


enum Provider {
	case send(ios: Bool, friendToken: String)
	case userRegOk(ios: Bool, friendToken: String)
	case location(ios: Bool, friendToken: String)
}

extension Provider: TargetType {
	var headers: [String : String]? {
		switch self {
		default:
			return ["Content-Type": "application/json",
					"Authorization": "key=\(DefineShare.fcmAppKey)"]
		}
	}
	
	var baseURL: URL {
		switch self {
		default:
			return URL(string: "https://fcm.googleapis.com")!
		}
	}
	
	var path: String {
		print(self)
		switch self {
		case .send:
			return "/fcm/send"
		case .location:
			return "/fcm/send"
		case .userRegOk:
			return "/fcm/send"
		}
	}
	
	var method: Moya.Method {
		switch self {
		default:
			return .post
		}
	}
	
	var sampleData: Data {
		return "".data(using: .utf8)!
	}

	var parameters: [String: Any] {
		var params = [String: Any]()
		
		let paramsNoti = [
			"title": "여기야 - 위치",
			"body": "",
			"sound": "default",
		]

		switch self {
		case .send(let ios, let friendToken):
			let paramsSend = [
				"pkg": "IamHere",
				"action": "reqLoc",
				"type": "ios",
				"reqKey": MyInfoModel.shared.getToken() ?? ""
			]
			
			if ios {
				params["notification"] = paramsNoti
			}
			
			params["to"] = friendToken
			params["data"] = paramsSend
			params["time_to_live"] = 5
			params["priority"] = "high"
			params["mutable_content"] = true
			
		case .location(let ios, let friendToken):
			let paramsGPS = [
				"providerName": "gps",
				"isFakeGps": "false",
				"latitude": UtilLocation.shared.currentLocationString().latitude,
				"longitude": UtilLocation.shared.currentLocationString().longitude,
				"time": Util.sendDate,
			]
			
			let paramsNetwork = [
				"providerName": "network",
				"isFakeGps": "false",
				"latitude": UtilLocation.shared.currentLocationString().latitude,
				"longitude": UtilLocation.shared.currentLocationString().longitude,
				"time": Util.sendDate,
			]

			let paramsLocation: [String: Any] = [
				"pkg": "IamHere",
				"action": "resLoc",
				"type": "ios",
				"resKey": MyInfoModel.shared.getToken() ?? "",
				"gps": paramsGPS,
				"network": paramsNetwork,
				"resCode": "1000"
			]

			if ios {
				params["notification"] = paramsNoti
			}
			
			params["to"] = friendToken
			params["data"] = paramsLocation
			params["time_to_live"] = 5
			params["priority"] = "high"
			params["mutable_content"] = true
			
		case .userRegOk(let ios, let friendToken):
			let paramsUserRegOk = [
				"pkg": "IamHere",
				"action": "userRegOk",
				"type": "ios",
				"reqKey": friendToken,
				"resKey": MyInfoModel.shared.getToken()
			]

			if ios {
				params["notification"] = paramsNoti
			}
			
			params["to"] = friendToken
			params["data"] = paramsUserRegOk
			params["time_to_live"] = 5
			params["priority"] = "high"
			params["mutable_content"] = true
		}
		
		return params
	}
	
	var task: Task {
		switch self {
		default:
			return .requestParameters(parameters: self.parameters, encoding: JSONEncoding.default)
		}
	}
}

