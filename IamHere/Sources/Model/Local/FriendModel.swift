//
//  FriendModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/02.
//

import UIKit


class FriendModel: NSObject {
	static let shared = FriendModel()
	
	private var friendList: [Friend]?

	
	func get() -> [Friend]? {
		self.load()

		return self.friendList
	}
	
	func add(token: String?, ios: Bool) -> Bool {
		guard let _token = token else {
			return false
		}
		
		var add = true
		
		self.load()

		self.friendList?.forEach {
			if $0.token == token {
				add = false
			}
		}
		
		if add {
			let friend = Friend(token: _token, name: String(_token.suffix(8)), ios: ios)

			self.friendList?.append(friend)
			
			self.save()
		}
		
		return add
	}
	
	func remove(friend: Friend) {
		self.load()

		self.friendList?.enumerated().forEach {
			if $0.element.token == friend.token {
				self.friendList?.remove(at: $0.offset)
				self.save()
			}
		}
	}
	
	func rename(token: String, name: String) {
		self.friendList?.enumerated().forEach {
			if $0.element.token == token {
				self.friendList?[$0.offset].name = name
				self.save()
			}
		}
	}
	
	func hasFriend(token: String) -> Bool {
		self.load()
		
		if let _list = self.friendList {
			for item in _list {
				if item.token == token {
					return true
				}
			}
		}
		
		return false
	}
}

private extension FriendModel {
	func load() {
		guard let _ = self.friendList else {
			if let _data = UserDefaults.standard.data(forKey: "userFriend") {
				let decoder = JSONDecoder()

				if let _friendList = try? decoder.decode([Friend].self, from: _data) {
					self.friendList = _friendList
				} else {
					self.friendList = [Friend]()
				}
			} else if let _data = UserDefaults(suiteName: "group.neo.iamhere")?.data(forKey: "userFriend") {
				let decoder = JSONDecoder()

				if let _friendList = try? decoder.decode([Friend].self, from: _data) {
					self.friendList = _friendList
				} else {
					self.friendList = [Friend]()
				}
			} else {
				self.friendList = [Friend]()
			}

			return
		}
	}
	
	func save() {
		let encoder = JSONEncoder()
		let save = try? encoder.encode(self.friendList)

		UserDefaults.standard.set(save, forKey: "userFriend")
		UserDefaults(suiteName: "group.neo.iamhere")?.set(save, forKey: "userFriend")
	}
}

extension FriendModel {
	struct Friend: Codable {
		let token: String
		var name: String
		var ios: Bool
	}
}

