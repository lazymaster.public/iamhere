//
//  MyInfoModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/15.
//

import UIKit

class MyInfoModel: NSObject {
	static let shared = MyInfoModel()
	
	private var info: MyInfo?

	
	func getToken() -> String? {
		self.load()

		return self.info?.token
	}
	
	func set(token: String?) {
		self.load()
		
		self.info?.token = token
		
		self.save()
	}
	
	func getFirstRun() -> Bool {
		self.load()

		return self.info?.firstRun ?? true
	}
	
	func set(firstRun: Bool) {
		self.load()
		
		self.info?.firstRun = firstRun
		
		self.save()
	}
}

private extension MyInfoModel {
	func load() {
		guard let _ = self.info else {
			if let _data = UserDefaults.standard.data(forKey: "myInfo") {
				let decoder = JSONDecoder()

				if let _info = try? decoder.decode(MyInfo.self, from: _data) {
					self.info = _info
				} else {
					self.info = MyInfo(token: nil, firstRun: true)
				}
			} else if let _data = UserDefaults(suiteName: "group.neo.iamhere")?.data(forKey: "myInfo") {
				let decoder = JSONDecoder()

				if let _info = try? decoder.decode(MyInfo.self, from: _data) {
					self.info = _info
				} else {
					self.info = MyInfo(token: nil, firstRun: true)
				}
			} else {
				self.info = MyInfo(token: nil, firstRun: true)
			}

			return
		}
	}
	
	func save() {
		let encoder = JSONEncoder()
		let save = try? encoder.encode(self.info)

		UserDefaults.standard.set(save, forKey: "myInfo")
		UserDefaults(suiteName: "group.neo.iamhere")?.set(save, forKey: "myInfo")
	}
}

extension MyInfoModel {
	struct MyInfo: Codable {
		var token: String?
		var firstRun: Bool
	}
}

