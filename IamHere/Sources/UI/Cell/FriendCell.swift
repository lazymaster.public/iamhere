//
//  FriendCell.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/02.
//

import UIKit


class FriendCell: UITableViewCell {
	@IBOutlet var viewContent: UIView? {
		didSet {
			self.viewContent?.setShadow(color: .black, alpha: 0.3, radius: 3, x: 0, y: 2)
		}
	}
	@IBOutlet var labelName: UILabel?
	@IBOutlet var labelProfile: UILabel? {
		didSet {
			self.labelProfile?.layer.cornerRadius = 18
			self.labelProfile?.clipsToBounds = true
		}
	}

	static let cellHeight: CGFloat = 48
//	static let cellSize = CGSize(width: UIScreen.main.bounds.width - 44, height: 48)

	
	override func layoutSubviews() {
		super.layoutSubviews()

		contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0))
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}

	func setCell(friend: FriendModel.Friend?) {
		guard let _friend = friend else {
			return
		}
		
		self.labelName?.text = "\(_friend.name) [\(_friend.token.suffix(8))]"
		self.labelProfile?.text = _friend.name
	}
}

