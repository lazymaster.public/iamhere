//
//  ViewController.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/12.
//

import UIKit
import GoogleMobileAds


class MainViewController: UIViewController {
	@IBOutlet weak var tableView: UITableView? {
		didSet {
			self.tableView?.register(UINib.init(nibName: "FriendCell", bundle: nil), forCellReuseIdentifier: "FriendCell")
		}
	}
	@IBOutlet weak var viewBanner: GADBannerView? {
		didSet {
			self.viewBanner?.adUnitID = "ca-app-pub-2414238337006076/8780996273"
			self.viewBanner?.rootViewController = self
			self.viewBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var buttonMyLocation: UIButton? {
		didSet {
			self.buttonMyLocation?.layer.cornerRadius = 6
		}
	}
	@IBOutlet weak var buttonAdd: UIButton? {
		didSet {
			self.buttonAdd?.layer.cornerRadius = 24
			self.buttonAdd?.setShadow(color: .black, alpha: 0.3, radius: 3, x: 1, y: 2)
		}
	}
	@IBOutlet weak var buttonInfo: UIButton?
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		MainViewModel.shared.initView(self)
		
		self.bind()
	}
}

private extension MainViewController {
	func bind() {
		MainViewModel.shared.bind(
			MainViewModel.Input(
				tapAdd: self.buttonAdd?.rx.tap.asObservable(),
				tapMyLocation: self.buttonMyLocation?.rx.tap.asObservable()
			)
		)
	}
}

