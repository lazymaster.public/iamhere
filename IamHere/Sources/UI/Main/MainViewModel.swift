//
//  MainViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/25.
//

import UIKit
import RxSwift
import RxCocoa


class MainViewModel: NSObject {
	static let shared = MainViewModel()
	
	var currentRootViewController: UIViewController?
	var currentViewController: MainViewController?
	private lazy var disposeBag = DisposeBag()
	private var friend: FriendModel.Friend?
	
	
	func show() {
		guard let _superViewController = self.topViewController() else {
			return
		}
		
		if let _viewController = self.currentRootViewController {
			_superViewController.present(_viewController, animated: false, completion: nil)
		} else {
			let sb = UIStoryboard(name: "Main", bundle: nil)
			
			if let _root = sb.instantiateViewController(withIdentifier: "MainRootViewController") as? UINavigationController,
			   let _viewController = _root.children.first as? MainViewController {
				self.currentViewController = _viewController
				self.currentRootViewController = _root

				DispatchQueue.main.async {
					_root.providesPresentationContextTransitionStyle = true
					_root.definesPresentationContext = true
					_root.modalPresentationStyle = .overFullScreen

					_superViewController.present(_root, animated: true, completion: nil)
				}
			}
		}
	}
	
	func dismiss() {
		guard let _viewController = self.currentViewController else {
			return
		}

		DispatchQueue.main.async {
			_viewController.dismiss(animated: false, completion: nil)
		}
	}
}

extension MainViewModel {
	func initView(_ viewController: MainViewController?) {
		self.currentViewController = viewController
		self.currentViewController?.tableView?.dataSource = self
		self.currentViewController?.tableView?.delegate = self
	}
	
	func updateView() {
		if let _ = DefineShare.fcmFriendToken {
			ShareUserViewModel.shared.push(self.currentViewController)
		}
	}
}

extension MainViewModel {
	var hasVisible: Bool {
		return self.currentViewController != nil
	}
	
	func reload() {
		self.currentViewController?.tableView?.reloadData()
	}
}

private extension MainViewModel {
	func tapAdd() {
		Util.kakaoLink()
	}
	
	func tapMyLocation() {
		MyLocationViewModel.shared.setInfo(latitude: nil, longitude: nil, time: nil)
		MyLocationViewModel.shared.push(self.currentViewController, friend: nil)
	}
}

extension MainViewModel {
	func bind(_ input: Input) {
		input.tapAdd?
			.bind {
				self.tapAdd()
			}
			.disposed(by: self.disposeBag)
		
		input.tapMyLocation?
			.bind {
				self.tapMyLocation()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapAdd: Observable<Void>?
		let tapMyLocation: Observable<Void>?
	}
}

extension MainViewModel: UITableViewDataSource {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return FriendCell.cellHeight
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return FriendModel.shared.get()?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! FriendCell

		if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
			let friend = _list[indexPath.row]

			cell.setCell(friend: friend)
		}

		return cell
	}
}

extension MainViewModel: UITableViewDelegate {
	func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
		let delete = UIContextualAction(style: .normal, title: "삭제") { action, view, completionHaldler in
			if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
				let friend = _list[indexPath.row]

				let alertController = UIAlertController(title: "", message: "\(friend.name)님을 삭제하시겠습니까?", preferredStyle: .alert)
				
				alertController.addAction(UIAlertAction(title: "취소", style: .default) { _ in
				})

				alertController.addAction(UIAlertAction(title: "확인", style: .default) { _ in
					FriendModel.shared.remove(friend: friend)
					
					self.reload()
				})
				
				self.currentViewController?.present(alertController, animated: true, completion: {})
			}
			
			completionHaldler(true)
		}
		delete.backgroundColor = .red
		
		let edit = UIContextualAction(style: .normal, title: "이름 변경") { action, view, completionHaldler in
			if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
				let friend = _list[indexPath.row]
				
				PopupAddViewModel.shared.show(editFriend: friend)
			}
			completionHaldler(true)
		}
		edit.backgroundColor = .blue
		
		return UISwipeActionsConfiguration(actions: [delete, edit])
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
			let friend = _list[indexPath.row]
			
			MyLocationViewModel.shared.push(self.currentViewController, friend: friend)
		}
	}
}

