//
//  LoadingViewModel.swift
//  AllStay_iOS
//
//  Created by LazyMaster.iMac on 2021/03/04.
//

import UIKit


public class LoadingViewModel: NSObject {
	// MARK: - let
	public static let shared = LoadingViewModel()

	// MARK: - var
	var currentViewController: LoadingViewController?
	
	
	public func show() {
		guard let superViewController = self.topViewController() else {
			return
		}
		
		if let _ = self.currentViewController {
			self.dismiss()
		}

		DispatchQueue.main.async {
			self.currentViewController = LoadingViewController(nibName: "LoadingViewController", bundle: nil)

			if let _viewController = self.currentViewController {
				_viewController.providesPresentationContextTransitionStyle = true
				_viewController.definesPresentationContext = true
				_viewController.modalPresentationStyle = .overFullScreen

				superViewController.present(_viewController, animated: false, completion: nil)
			}
		}
	}
	
	public func dismiss(_ completion: (() -> ())? = nil) {
		guard let _viewController = self.currentViewController else {
			self.currentViewController = nil
			
			completion?()
			return
		}
		
		_viewController.dismiss(animated: false, completion: {
			self.currentViewController = nil
			
			completion?()
		})
	}
}

extension LoadingViewModel {
	func initUI() {
		self.currentViewController?.viewIndicator?.startAnimating()
	}
}
