//
//  LoadingViewController.swift
//  AllStay_iOS
//
//  Created by LazyMaster.iMac on 2021/03/04.
//

import UIKit
import NVActivityIndicatorView


class LoadingViewController: UIViewController {
	@IBOutlet weak var viewIndicator: NVActivityIndicatorView?

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		LoadingViewModel.shared.initUI()
    }
}

