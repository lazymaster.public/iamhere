//
//  Term2ViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/24.
//

import UIKit
import RxSwift
import RxCocoa


class Term2ViewModel: NSObject {
	static let shared = Term2ViewModel()
	
	var currentViewController: Term2ViewController?
	private lazy var disposeBag = DisposeBag()
	var goToMainCheck = false
	
	
	func show() {
		guard let _superViewController = self.topViewController() else {
			return
		}
		
		if let _viewController = self.currentViewController {
			_superViewController.present(_viewController, animated: false, completion: nil)
		} else {
			let sb = UIStoryboard(name: "Main", bundle: nil)
			
			if let _viewController = sb.instantiateViewController(withIdentifier: "Term2ViewController") as? Term2ViewController {
				_viewController.providesPresentationContextTransitionStyle = true
				_viewController.definesPresentationContext = true
				_viewController.modalPresentationStyle = .overFullScreen
				_viewController.modalPresentationCapturesStatusBarAppearance = true

				self.currentViewController = _viewController

				_superViewController.present(_viewController, animated: false, completion: nil)
			}
		}
	}
	
	func dismiss(completion: @escaping (() -> ())) {
		guard let _viewController = self.currentViewController else {
			return
		}

		DispatchQueue.main.async {
			_viewController.dismiss(animated: false, completion: completion)
		}
	}
}

extension Term2ViewModel {
	func initView(_ viewController: Term2ViewController?) {
		self.currentViewController = viewController
	}
	
	func updateView() {
	}
}

extension Term2ViewModel {
	var isVisible: Bool {
		if let _viewController = self.currentViewController {
			return self.topViewController() == _viewController
		} else {
			return false
		}
	}
}

private extension Term2ViewModel {
	func tapTerm() {
		Util.goAppSetting {
			self.goToMainCheck = true
		}
	}
}

extension Term2ViewModel {
	func bind(_ input: Input) {
		input.tapTerm?
			.bind {
				self.tapTerm()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapTerm: Observable<Void>?
	}
}

