//
//  InfoViewController.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/25.
//

import UIKit
import GoogleMobileAds


class InfoViewController: UIViewController {
	@IBOutlet weak var viewBanner: GADBannerView? {
		didSet {
			self.viewBanner?.adUnitID = "ca-app-pub-2414238337006076/8780996273"
			self.viewBanner?.rootViewController = self
			self.viewBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var buttonBack: UIButton?
	@IBOutlet weak var buttonGoSetting: UIButton? {
		didSet {
			self.buttonGoSetting?.layer.cornerRadius = 6
		}
	}
	@IBOutlet weak var buttonID: UIButton? {
		didSet {
			if let _key = MyInfoModel.shared.getToken()?.suffix(8) {
				let id = String(_key)
				
				self.buttonID?.setTitle(id, for: .normal)
			}
			
			self.buttonID?.layer.cornerRadius = 6
		}
	}
	@IBOutlet weak var buttonHelp: UIButton? {
		didSet {
			self.buttonHelp?.layer.cornerRadius = 6
		}
	}
	@IBOutlet weak var labelVersion: UILabel? {
		didSet {
			self.labelVersion?.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
		}
	}

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		InfoViewModel.shared.initView(self)
		
		self.bind()
	}
}

private extension InfoViewController {
	func bind() {
		InfoViewModel.shared.bind(
			InfoViewModel.Input(
				tapBack: self.buttonBack?.rx.tap.asObservable(),
				tapGoSetting: self.buttonGoSetting?.rx.tap.asObservable()
			)
		)
	}
}

