//
//  InfoViewModel.swift
//  IamHere
//
//  Created by LazyMaster on 2021/11/25.
//

import UIKit
import RxSwift
import RxCocoa


class InfoViewModel: NSObject {
	static let shared = InfoViewModel()
	
	private var currentViewController: InfoViewController?
	private lazy var disposeBag = DisposeBag()
}

extension InfoViewModel {
	func initView(_ viewController: InfoViewController?) {
		self.currentViewController = viewController
	}
	
	func pop() {
		guard let _navigation = self.currentViewController?.navigationController else {
			return
		}
		
		_navigation.popViewController(animated: true)
	}
}

private extension InfoViewModel {
	func tapBack() {
		self.pop()
	}
	
	func tapGoSetting() {
		Util.goAppSetting {
			Term2ViewModel.shared.goToMainCheck = false
		}
	}
}

extension InfoViewModel {
	func bind(_ input: Input) {
		input.tapBack?
			.bind {
				self.tapBack()
			}
			.disposed(by: self.disposeBag)
		
		input.tapGoSetting?
			.bind {
				self.tapGoSetting()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapBack: Observable<Void>?
		let tapGoSetting: Observable<Void>?
	}
}

