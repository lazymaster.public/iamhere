//
//  MyLocationViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/25.
//

import UIKit
import RxSwift
import RxCocoa


class MyLocationViewModel: NSObject {
	static let shared = MyLocationViewModel()
	
	private var currentViewController: MyLocationViewController?
	private lazy var disposeBag = DisposeBag()
	private var mapView: MTMapView?
	private var friend: FriendModel.Friend?
	private var latitude: Double?
	private var longitude: Double?
	private var time: String?
	private var timer: Timer?
	private var timerProgress: Timer?
	private var progress: Float = 0

	
	func push(_ superViewController: UIViewController?, friend: FriendModel.Friend?) {
		guard let _navigation = superViewController?.navigationController else { return }
		
		let sb = UIStoryboard(name: "Main", bundle: nil)
		let viewController = sb.instantiateViewController(withIdentifier: "MyLocationViewController")
		
		_navigation.pushViewController(viewController, animated: true)
		
		self.friend = friend
	}
}

extension MyLocationViewModel {
	func initView(_ viewController: MyLocationViewController?) {
		self.currentViewController = viewController
		
		self.initMap()
	}
	
	func updateView() {
		self.currentViewController?.viewInfo?.isHidden = true
		self.currentViewController?.viewError?.isHidden = true

		self.load()
	}
	
	func pop() {
		guard let _navigation = self.currentViewController?.navigationController else {
			return
		}
		
		_navigation.popViewController(animated: true)
	}
}

private extension MyLocationViewModel {
	func initMap() {
		guard let _viewController = self.currentViewController else {
			return
		}
		
		if let _mapBounds = _viewController.viewMap?.bounds {
			self.mapView = MTMapView(frame: _mapBounds)
			
			if let _mapView = self.mapView {
				_mapView.baseMapType = .standard
				self.currentViewController?.viewMap?.addSubview(_mapView)
			}
		}
	}
	
	func load() {
		if let _friend = self.friend {
			self.currentViewController?.labelTitle?.text = "위치 확인 [\(_friend.name)]"

			self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.error), userInfo: nil, repeats: false)

			self.showProgress()

			SendModel.request(ios: _friend.ios, friendToken: _friend.token)
				.subscribe(onNext: { _ in
				}, onError: { _ in
					self.friend = nil
					self.hideProgress()
				})
				.disposed(by: self.disposeBag)
		} else {
			self.currentViewController?.labelTitle?.text = "내 위치 전송"

			self.setLocation()
		}
	}
	
	@objc func error() {
		self.timer?.invalidate()
		self.timer = nil
		
		self.currentViewController?.viewError?.isHidden = false
		self.hideProgress()
	}
	
	func showProgress() {
		guard let _viewController = self.currentViewController else {
			return
		}
		
		self.hideProgress()

		self.timerProgress = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.setProgress), userInfo: nil, repeats: true)
		
		_viewController.viewProgress?.progress = 0
		_viewController.viewProgressBox?.isHidden = false
	}
	
	func hideProgress() {
		guard let _viewController = self.currentViewController else {
			return
		}
		
		self.progress = 0

		_viewController.viewProgressBox?.isHidden = true
		
		self.timerProgress?.invalidate()
		self.timerProgress = nil
	}
	
	@objc func setProgress() {
		guard let _viewController = self.currentViewController else {
			return
		}
		
		self.progress += 0.1
		
		_viewController.viewProgress?.progress = self.progress / 20
	}
}

extension MyLocationViewModel {
	func setInfo(latitude: Double?, longitude: Double?, time: String?) {
		self.latitude = latitude
		self.longitude = longitude
		self.time = time
		
		self.timer?.invalidate()
		self.timer = nil

		self.hideProgress()
	}
	
	func setLocation() {
		let latitude: Double
		let longitude: Double
		let name: String

		if let _friend = self.friend, let _latitude = self.latitude, let _longitude = self.longitude {
			name = _friend.name
			latitude = _latitude
			longitude = _longitude
			
			self.currentViewController?.viewInfo?.isHidden = false
			self.currentViewController?.viewError?.isHidden = true
		} else {
			name = "내 위치"
			latitude = UtilLocation.shared.currentLocation().latitude
			longitude = UtilLocation.shared.currentLocation().longitude
			
			self.currentViewController?.viewInfo?.isHidden = true
			self.currentViewController?.viewError?.isHidden = true
		}

		self.mapView?.removeAllPOIItems()
		
		let geo = MTMapPointGeo(latitude: latitude, longitude: longitude)
		let point = MTMapPoint(geoCoord: geo)
		let poiItem = MTMapPOIItem()
		
		poiItem.itemName = "\(name)\n\(Util.receiveDate(time: self.time))"
		poiItem.mapPoint = point
		poiItem.markerType = .customImage
		poiItem.customImageName = "poiItemPin.png"
		poiItem.showAnimationType = .springFromGround
		poiItem.showDisclosureButtonOnCalloutBalloon = false
		poiItem.draggable = false
		poiItem.tag = 1000
		
		self.mapView?.setMapCenter(point, zoomLevel: -1, animated: true)
		self.mapView?.add(poiItem)
		self.mapView?.select(poiItem, animated: true)
	}
}

private extension MyLocationViewModel {
	func tapBack() {
		self.pop()
	}
	
	func tapAdd() {
		self.load()
	}
}

extension MyLocationViewModel {
	func bind(_ input: Input) {
		input.tapBack?
			.bind {
				self.tapBack()
			}
			.disposed(by: self.disposeBag)
		
		input.tapAdd?
			.bind {
				self.tapAdd()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapBack: Observable<Void>?
		let tapAdd: Observable<Void>?
	}
}

