//
//  MyLocationViewController.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/25.
//

import UIKit
import GoogleMobileAds


class MyLocationViewController: UIViewController {
	@IBOutlet weak var viewMap: UIView?
	@IBOutlet weak var viewInfo: UIView?
	@IBOutlet weak var viewError: UIView?
	@IBOutlet weak var viewBanner: GADBannerView? {
		didSet {
			self.viewBanner?.adUnitID = "ca-app-pub-2414238337006076/8780996273"
			self.viewBanner?.rootViewController = self
			self.viewBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var viewProgressBox: UIView? {
		didSet {
			self.viewProgressBox?.isHidden = true
		}
	}
	@IBOutlet weak var viewProgress: UIProgressView? {
		didSet {
			self.viewProgress?.progressTintColor = .systemBlue
		}
	}
	@IBOutlet weak var viewProgressBanner: GADBannerView? {
		didSet {
			self.viewProgressBanner?.adUnitID = "ca-app-pub-2414238337006076/5691798503"
			self.viewProgressBanner?.rootViewController = self
			self.viewProgressBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var labelTitle: UILabel?
	@IBOutlet weak var buttonBack: UIButton?
	@IBOutlet weak var buttonAdd: UIButton? {
		didSet {
			self.buttonAdd?.layer.cornerRadius = 24
			self.buttonAdd?.setShadow(color: .black, alpha: 0.3, radius: 3, x: 1, y: 2)
		}
	}

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		MyLocationViewModel.shared.initView(self)
		
		self.bind()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		MyLocationViewModel.shared.updateView()
	}
}

private extension MyLocationViewController {
	func bind() {
		MyLocationViewModel.shared.bind(
			MyLocationViewModel.Input(
				tapBack: self.buttonBack?.rx.tap.asObservable(),
				tapAdd: self.buttonAdd?.rx.tap.asObservable()
			)
		)
	}
}

