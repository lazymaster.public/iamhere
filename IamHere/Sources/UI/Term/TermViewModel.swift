//
//  TermViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/24.
//

import UIKit
import RxSwift
import RxCocoa
import AppTrackingTransparency


class TermViewModel: NSObject {
	static let shared = TermViewModel()
	
	var currentViewController: TermViewController?
	private lazy var disposeBag = DisposeBag()
	
	
	func show() {
		guard let _superViewController = self.topViewController() else {
			return
		}
		
		if let _viewController = self.currentViewController {
			_superViewController.present(_viewController, animated: false, completion: nil)
		} else {
			let sb = UIStoryboard(name: "Main", bundle: nil)
			
			if let _viewController = sb.instantiateViewController(withIdentifier: "TermViewController") as? TermViewController {
				_viewController.providesPresentationContextTransitionStyle = true
				_viewController.definesPresentationContext = true
				_viewController.modalPresentationStyle = .overFullScreen
				_viewController.modalPresentationCapturesStatusBarAppearance = true

				self.currentViewController = _viewController

				_superViewController.present(_viewController, animated: false, completion: nil)
			}
		}
	}
	
	func dismiss(completion: @escaping (() -> ())) {
		guard let _viewController = self.currentViewController else {
			return
		}

		DispatchQueue.main.async {
			_viewController.dismiss(animated: false, completion: completion)
			self.currentViewController = nil
		}
	}
}

extension TermViewModel {
	func initView(_ viewController: TermViewController?) {
		self.currentViewController = viewController
	}
	
	func updateView() {
	}
}

extension TermViewModel {
	var isVisible: Bool {
		if let _viewController = self.currentViewController {
			return self.topViewController() == _viewController
		} else {
			return false
		}
	}
}

private extension TermViewModel {
	func requestTerm() {
		UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) { (granted, error) in
			guard granted else { return }
			
			DispatchQueue.main.async {
				UIApplication.shared.registerForRemoteNotifications()
				
				UtilLocation.shared.request { _ in
					if self.currentViewController != nil {
						self.dismiss {
							if UtilLocation.shared.checkLocationAlways() {
								MainViewModel.shared.show()
							} else {
								Term2ViewModel.shared.show()
							}
						}
					}
				}
			}
		}
	}
}

private extension TermViewModel {
	func tapTerm() {
		MyInfoModel.shared.set(firstRun: false)
		
		if #available(iOS 14, *) {
			ATTrackingManager.requestTrackingAuthorization { _ in
				self.requestTerm()
			}
		} else {
			self.requestTerm()
		}

	}
}

extension TermViewModel {
	func bind(_ input: Input) {
		input.tapTerm?
			.bind {
				self.tapTerm()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapTerm: Observable<Void>?
	}
}

