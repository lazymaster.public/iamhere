//
//  TermViewController.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/24.
//

import UIKit


class TermViewController: UIViewController {
	@IBOutlet weak var buttonTerm: UIButton? {
		didSet {
			self.buttonTerm?.layer.cornerRadius = 24
			self.buttonTerm?.setShadow(color: .black, alpha: 0.3, radius: 3, x: 1, y: 2)
		}
	}

	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		TermViewModel.shared.initView(self)
		
		self.bind()
	}
}

private extension TermViewController {
	func bind() {
		TermViewModel.shared.bind(
			TermViewModel.Input(
				tapTerm: self.buttonTerm?.rx.tap.asObservable()
			)
		)
	}
}

