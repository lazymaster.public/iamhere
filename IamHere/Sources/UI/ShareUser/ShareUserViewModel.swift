//
//  ShareUserViewModel.swift
//  IamHere
//
//  Created by LazyMaster on 2021/11/25.
//

import UIKit
import RxSwift
import RxCocoa


class ShareUserViewModel: NSObject {
	static let shared = ShareUserViewModel()
	
	private var currentViewController: ShareUserViewController?
	private lazy var disposeBag = DisposeBag()
	private var friend: FriendModel.Friend?

	
	func push(_ superViewController: UIViewController?) {
		guard let _navigation = superViewController?.navigationController else { return }
		
		let sb = UIStoryboard(name: "Main", bundle: nil)
		let viewController = sb.instantiateViewController(withIdentifier: "ShareUserViewController")
		
		_navigation.pushViewController(viewController, animated: true)
	}
}

extension ShareUserViewModel {
	func initView(_ viewController: ShareUserViewController?) {
		self.currentViewController = viewController
		self.currentViewController?.tableView?.dataSource = self
		self.currentViewController?.tableView?.delegate = self
	}
	
	func updateView() {
		if let _ = DefineShare.fcmFriendToken {
			PopupAddViewModel.shared.show()
		}
	}
	
	func pop() {
		guard let _navigation = self.currentViewController?.navigationController else {
			return
		}
		
		_navigation.popViewController(animated: true)
	}
}

extension ShareUserViewModel {
	func reload() {
		self.currentViewController?.tableView?.reloadData()
	}
}

private extension ShareUserViewModel {
	func tapBack() {
		self.pop()
	}
	
	func tapAdd() {
		Util.kakaoLink()
	}
}

extension ShareUserViewModel {
	func bind(_ input: Input) {
		input.tapBack?
			.bind {
				self.tapBack()
			}
			.disposed(by: self.disposeBag)
		
		input.tapAdd?
			.bind {
				self.tapAdd()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapBack: Observable<Void>?
		let tapAdd: Observable<Void>?
	}
}

extension ShareUserViewModel: UITableViewDataSource {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return FriendCell.cellHeight
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return FriendModel.shared.get()?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! FriendCell

		if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
			let friend = _list[indexPath.row]

			cell.setCell(friend: friend)
		}

		return cell
	}
}

extension ShareUserViewModel: UITableViewDelegate {
	func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
		let delete = UIContextualAction(style: .normal, title: "삭제") { action, view, completionHaldler in
			if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
				let friend = _list[indexPath.row]

				
				let alertController = UIAlertController(title: "", message: "\(friend.name)님을 삭제하시겠습니까?", preferredStyle: .alert)
				
				alertController.addAction(UIAlertAction(title: "취소", style: .default) { _ in
				})

				alertController.addAction(UIAlertAction(title: "확인", style: .default) { _ in
					FriendModel.shared.remove(friend: friend)
					
					self.reload()
					MainViewModel.shared.reload()
				})
				
				self.currentViewController?.present(alertController, animated: true, completion: {})
			}
			
			completionHaldler(true)
		}
		delete.backgroundColor = .red
		
		let edit = UIContextualAction(style: .normal, title: "이름 변경") { action, view, completionHaldler in
			if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
				let friend = _list[indexPath.row]
				
				PopupAddViewModel.shared.show(editFriend: friend)
			}
			completionHaldler(true)
		}
		edit.backgroundColor = .blue
		
		return UISwipeActionsConfiguration(actions: [delete, edit])
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let _list = FriendModel.shared.get(), _list.count > indexPath.row {
			let friend = _list[indexPath.row]
			
			MyLocationViewModel.shared.push(self.currentViewController, friend: friend)
		}
	}
}


