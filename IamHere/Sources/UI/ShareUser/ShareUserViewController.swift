//
//  ShareUserViewController.swift
//  IamHere
//
//  Created by LazyMaster on 2021/11/25.
//

import UIKit
import GoogleMobileAds


class ShareUserViewController: UIViewController {
	@IBOutlet weak var tableView: UITableView? {
		didSet {
			self.tableView?.register(UINib.init(nibName: "FriendCell", bundle: nil), forCellReuseIdentifier: "FriendCell")
		}
	}
	@IBOutlet weak var viewBanner: GADBannerView? {
		didSet {
			self.viewBanner?.adUnitID = "ca-app-pub-2414238337006076/8780996273"
			self.viewBanner?.rootViewController = self
			self.viewBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var buttonBack: UIButton?
	@IBOutlet weak var buttonAdd: UIButton? {
		didSet {
			self.buttonAdd?.layer.cornerRadius = 24
			self.buttonAdd?.setShadow(color: .black, alpha: 0.3, radius: 3, x: 1, y: 2)
		}
	}
	@IBOutlet weak var labelNoContent: UILabel?

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		ShareUserViewModel.shared.initView(self)
		
		self.bind()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		ShareUserViewModel.shared.updateView()
	}
}

private extension ShareUserViewController {
	func bind() {
		ShareUserViewModel.shared.bind(
			ShareUserViewModel.Input(
				tapBack: self.buttonBack?.rx.tap.asObservable(),
				tapAdd: self.buttonAdd?.rx.tap.asObservable()
			)
		)
	}
}

