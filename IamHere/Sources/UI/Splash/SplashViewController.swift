//
//  SplashViewController.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/24.
//

import UIKit


class SplashViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
		
		SplashViewModel.shared.initView(self)
	}
}

