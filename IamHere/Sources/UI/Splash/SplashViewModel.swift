//
//  SplashViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/12/24.
//

import UIKit
import GoogleMobileAds


class SplashViewModel: NSObject {
	static let shared = SplashViewModel()
	
	var currentViewController: SplashViewController?
}

extension SplashViewModel {
	func initView(_ viewController: SplashViewController?) {
		self.currentViewController = viewController

//		GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["558e3c28167059e3f02f8648541dd68b"]
		GADMobileAds.sharedInstance().start { status in
			DispatchQueue.main.async {
				if MyInfoModel.shared.getFirstRun() {
					TermViewModel.shared.show()
				} else {
					if UtilLocation.shared.checkLocationAlways() {
						MainViewModel.shared.show()
					} else {
						Term2ViewModel.shared.show()
					}
				}
			}
		}
	}
}

extension SplashViewModel {
	var isVisible: Bool {
		if let _viewController = self.currentViewController {
			return self.topViewController() == _viewController
		} else {
			return false
		}
	}
}

