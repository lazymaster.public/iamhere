//
//  HelpViewModel.swift
//  IamHere
//
//  Created by LazyMaster on 2021/11/25.
//

import UIKit
import RxSwift
import RxCocoa


class HelpViewModel: NSObject {
	static let shared = HelpViewModel()
	
	private var currentViewController: HelpViewController?
	private lazy var disposeBag = DisposeBag()
}

extension HelpViewModel {
	func initView(_ viewController: HelpViewController?) {
		self.currentViewController = viewController
	}
	
	func pop() {
		guard let _navigation = self.currentViewController?.navigationController else {
			return
		}
		
		_navigation.popViewController(animated: true)
	}
}

private extension HelpViewModel {
	func tapBack() {
		self.pop()
	}
	
	func tapGoSetting() {
		Util.goAppSetting {
			Term2ViewModel.shared.goToMainCheck = false
		}
	}
}

extension HelpViewModel {
	func bind(_ input: Input) {
		input.tapBack?
			.bind {
				self.tapBack()
			}
			.disposed(by: self.disposeBag)
		
		input.tapGoSetting?
			.bind {
				self.tapGoSetting()
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapBack: Observable<Void>?
		let tapGoSetting: Observable<Void>?
	}
}

