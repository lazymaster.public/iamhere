//
//  HelpViewController.swift
//  IamHere
//
//  Created by LazyMaster on 2021/11/25.
//

import UIKit
import GoogleMobileAds


class HelpViewController: UIViewController {
	@IBOutlet weak var viewBanner: GADBannerView? {
		didSet {
			self.viewBanner?.adUnitID = "ca-app-pub-2414238337006076/8780996273"
			self.viewBanner?.rootViewController = self
			self.viewBanner?.load(GADRequest())
		}
	}
	@IBOutlet weak var buttonBack: UIButton?
	@IBOutlet weak var buttonGoSetting: UIButton? {
		didSet {
			self.buttonGoSetting?.layer.cornerRadius = 6
		}
	}

	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		HelpViewModel.shared.initView(self)
		
		self.bind()
	}
}

private extension HelpViewController {
	func bind() {
		HelpViewModel.shared.bind(
			HelpViewModel.Input(
				tapBack: self.buttonBack?.rx.tap.asObservable(),
				tapGoSetting: self.buttonGoSetting?.rx.tap.asObservable()
			)
		)
	}
}

