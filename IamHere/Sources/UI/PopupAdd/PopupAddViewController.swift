//
//  PopupAddViewController.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/26.
//

import UIKit


class PopupAddViewController: UIViewController {
	@IBOutlet var labelTitle: UILabel?
	@IBOutlet var textName: UITextField?
	@IBOutlet var buttonAdd: UIButton?
	@IBOutlet var buttonCancel: UIButton?

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		PopupAddViewModel.shared.initView()
		
		self.bind()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		PopupAddViewModel.shared.updateView()
	}
}

extension PopupAddViewController {
	func bind() {
		PopupAddViewModel.shared.bind(
			PopupAddViewModel.Input(
				tapAdd: self.buttonAdd?.rx.tap.asObservable(),
				tapCancel: self.buttonCancel?.rx.tap.asObservable(),
				textName: self.textName?.rx.text.orEmpty.asObservable()
			)
		)
	}
}

