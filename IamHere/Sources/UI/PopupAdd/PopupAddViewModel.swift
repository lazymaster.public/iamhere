//
//  PopupAddViewModel.swift
//  IamHere
//
//  Created by LazyMaster.iMac on 2021/11/26.
//

import UIKit
import RxSwift
import RxCocoa


class PopupAddViewModel: NSObject {
	static let shared = PopupAddViewModel()
	
	private var currentViewController: PopupAddViewController?
	private lazy var disposeBag = DisposeBag()
	
	private var friendName: String = ""
	private var editFriend: FriendModel.Friend?
	
	
	func show(editFriend: FriendModel.Friend? = nil) {
		guard let _superViewController = self.topViewController() else {
			return
		}
		
		if let _viewController = self.currentViewController {
			_superViewController.present(_viewController, animated: false, completion: nil)
		} else {
			let sb = UIStoryboard(name: "Main", bundle: nil)
			
			if let _viewController = sb.instantiateViewController(withIdentifier: "PopupAddViewController") as? PopupAddViewController {
				_viewController.providesPresentationContextTransitionStyle = true
				_viewController.definesPresentationContext = true
				_viewController.modalPresentationStyle = .overFullScreen
				_viewController.modalPresentationCapturesStatusBarAppearance = true

				self.currentViewController = _viewController

				_superViewController.present(_viewController, animated: false, completion: nil)
			}
		}
		
		self.editFriend = editFriend
	}
	
	func dismiss() {
		guard let _viewController = self.currentViewController else {
			return
		}

		DispatchQueue.main.async {
			_viewController.dismiss(animated: false, completion: nil)
		}
	}
}

extension PopupAddViewModel {
	func initView() {
	}
	
	func updateView() {
		self.friendName = self.editFriend?.name ?? DefineShare.fcmFriendName ?? ""
		self.currentViewController?.textName?.text = self.friendName
		
		if self.editFriend == nil {
			self.currentViewController?.labelTitle?.text = "사용자 추가"
			self.currentViewController?.buttonAdd?.setTitle("추가", for: .normal)
		} else {
			self.currentViewController?.labelTitle?.text = "이름 변경"
			self.currentViewController?.buttonAdd?.setTitle("변경", for: .normal)
		}
	}
}

private extension PopupAddViewModel {
	func tapAdd() {
		guard self.friendName.count > 0 else {
			return
		}
		
		if let _friend = self.editFriend {
			self.editFriend = nil
			
			FriendModel.shared.rename(token: _friend.token, name: self.friendName)
		} else {
			if let _token = DefineShare.fcmFriendToken {
				DefineShare.fcmFriendToken = nil
				DefineShare.fcmFriendName = nil
				
				FriendModel.shared.rename(token: _token, name: self.friendName)
			}
		}
		
		MainViewModel.shared.reload()
		ShareUserViewModel.shared.reload()

		self.dismiss()
	}
	
	func tapCancel() {
		self.editFriend = nil

		DefineShare.fcmFriendToken = nil
		DefineShare.fcmFriendName = nil
		
		MainViewModel.shared.reload()
		ShareUserViewModel.shared.reload()

		self.dismiss()
	}
	
	func text(name: String) {
		self.friendName = name
	}
}

extension PopupAddViewModel {
	func bind(_ input: Input) {
		input.tapAdd?
			.bind {
				self.tapAdd()
			}
			.disposed(by: self.disposeBag)
		
		input.tapCancel?
			.bind {
				self.tapCancel()
			}
			.disposed(by: self.disposeBag)

		input.textName?
			.bind {
				self.text(name: $0)
			}
			.disposed(by: self.disposeBag)
	}
	
	struct Input {
		let tapAdd: Observable<Void>?
		let tapCancel: Observable<Void>?
		let textName: Observable<String>?
	}
}

