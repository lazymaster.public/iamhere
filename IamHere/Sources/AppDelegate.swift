//
//  AppDelegate.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/12.
//

import UIKit
import Firebase
import KakaoSDKCommon
import RxSwift


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	private lazy var disposeBag = DisposeBag()


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		FirebaseApp.configure()

		Messaging.messaging().delegate = self
		UNUserNotificationCenter.current().delegate = self
		
		KakaoSDK.initSDK(appKey: "1d9151d60bf87f258855958c6cfde4b3")
		
		self.loadPayload(userInfo: launchOptions as? [String: Any])
		
		return true
	}

	// active -> inactive
	func applicationWillResignActive(_ application: UIApplication) {
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
	}

	// inactive -> active
	func applicationDidBecomeActive(_ application: UIApplication) {
		let splashVisible = SplashViewModel.shared.isVisible
		let termVisible = TermViewModel.shared.isVisible
		let term2Visible = Term2ViewModel.shared.isVisible
		let always = UtilLocation.shared.checkLocationAlways()

		if Term2ViewModel.shared.goToMainCheck {
			Term2ViewModel.shared.goToMainCheck = false

			if always {
				Term2ViewModel.shared.dismiss {
					if !MainViewModel.shared.hasVisible {
						MainViewModel.shared.show()
					}
				}
			}
		} else if !splashVisible, !termVisible, !term2Visible, !always {
			Term2ViewModel.shared.show()
		} else if term2Visible, always {
			Term2ViewModel.shared.dismiss {

			}
		}
	}

	func applicationWillTerminate(_ application: UIApplication) {
	}

	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		let listString = url.absoluteString.split(separator: "?")
		let list = listString.last?.split(separator: "&")
		var dict = [String: String]()

		list?.forEach({
			let data = $0.split(separator: "=")
			
			if let _key = data.first, let _value = data.last {
				dict[String(_key)] = String(_value)
			}
		})

		if dict["action"] == "shreReqLocation" {
			let token = dict["pkey"]
			let ios = dict["type"] == "ios"
			
			// 테스트를 위해 막아놓음
			if MyInfoModel.shared.getToken() == token {
				let alertController = UIAlertController(title: "", message: "자기 자신은 등록할 수 없습니다.", preferredStyle: .alert)

				alertController.addAction(UIAlertAction(title: "확인", style: .default) { _ in
				})

				self.topViewController()?.present(alertController, animated: true, completion: {})

				DefineShare.fcmFriendToken = nil
				DefineShare.fcmFriendName = nil
			} else {
				if let _token = token {
					let name = String(_token.suffix(8))

					UserRegOk.request(ios: ios, friendToken: _token)
						.subscribe {
							print($0)
						} onError: {
							print($0)
						}
						.disposed(by: self.disposeBag)

					if FriendModel.shared.add(token: _token, ios: ios) {
						DispatchQueue.main.async {
							DefineShare.fcmFriendToken = _token
							DefineShare.fcmFriendName = name
							
							self.popToRoot()
							
							ShareUserViewModel.shared.push(MainViewModel.shared.currentViewController)
						}
					} else {
						DefineShare.fcmFriendToken = nil
						DefineShare.fcmFriendName = nil

						let alertController = UIAlertController(title: "", message: "이미 등록된 사용자입니다.", preferredStyle: .alert)

						alertController.addAction(UIAlertAction(title: "확인", style: .default) { _ in
						})

						self.topViewController()?.present(alertController, animated: true, completion: {})
					}
				} else {
					DefineShare.fcmFriendToken = nil
					DefineShare.fcmFriendName = nil
				}
			}
		} else {
			DefineShare.fcmFriendToken = nil
			DefineShare.fcmFriendName = nil
		}

		return false
	}
	
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		let deviceTokenString: String = deviceToken.reduce("", { $0 + String(format: "%02X", $1) })

		Messaging.messaging().apnsToken = deviceToken
		
		print("등록된 토큰은 \(deviceTokenString) 입니다.")
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
	}
}

extension AppDelegate: UNUserNotificationCenterDelegate {
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		self.loadPayload(userInfo: notification.request.content.userInfo as? [String: Any])
		
		if #available(iOS 14.0, *) {
			completionHandler([.list, .badge, .sound])
		} else {
			completionHandler([.alert, .badge, .sound])
		}
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		let application = UIApplication.shared
		
		switch application.applicationState {
		case .active:
			break
		case .inactive:
			break
		case .background:
			break
		default:
			break
		}

		completionHandler()
	}
}

extension AppDelegate: MessagingDelegate {
	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
		if let _fcmToken = fcmToken {
			let dataDict: [String: String] = ["token": _fcmToken]
			print("[\(_fcmToken)]")
			MyInfoModel.shared.set(token: _fcmToken)

			NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
		}
	}
}

extension AppDelegate {
	func loadPayload(userInfo: [String: Any]?) {
		guard let _userInfo = userInfo else {
			return
		}
		
		guard let _action = _userInfo["action"] as? String else {
			return
		}
		
		print("action: \(_action)")

		// 상대방의 위치 받음
		if _action == "resLoc" {
			let gps = self.getGPS(gpsString: _userInfo["gps"] as? String)
			let network = self.getGPS(gpsString: _userInfo["network"] as? String)
			let location: (latitude: Double, longitude: Double, time: String?)?
			let time: String?

			if self.compare(gps: gps?.time, network: network?.time) {
				location = gps
				time = gps?.time
			} else {
				location = network
				time = network?.time
			}
			
			if let _location = location {
				MyLocationViewModel.shared.setInfo(latitude: _location.latitude, longitude: _location.longitude, time: time)
				MyLocationViewModel.shared.setLocation()
			}
		} else if _action == "reqLoc" {
			guard let _reqKey = _userInfo["reqKey"] as? String else {
				return
			}
			
			guard FriendModel.shared.hasFriend(token: _reqKey) else {
				return
			}
			
			let type = _userInfo["type"] as? String ?? "android"
			
			LocationModel.request(ios: type == "ios", friendToken: _reqKey)
				.subscribe {
					print($0)
				} onError: {
					print($0)
				}
				.disposed(by: self.disposeBag)
		} else if _action == "userRegOk" {
			if let _reqToken = _userInfo["reqKey"] as? String, let _resToken = _userInfo["resKey"] as? String, let _type = _userInfo["type"] as? String {
				if _reqToken == MyInfoModel.shared.getToken() {
					if FriendModel.shared.add(token: _resToken, ios: _type == "ios") {
						MainViewModel.shared.reload()
						ShareUserViewModel.shared.reload()
					}
				}
			}
		}
	}
	
	func getGPS(gpsString: String?) -> (latitude: Double, longitude: Double, time: String?)? {
		if let _gpsString = gpsString, !_gpsString.isEmpty {
			let _gpsList1 = _gpsString.replacingOccurrences(of: "{", with: "")
			let _gpsList2 = _gpsList1.replacingOccurrences(of: "}", with: "")
			let _gpsList3 = _gpsList2.replacingOccurrences(of: "\"", with: "")
			let listString = _gpsList3.split(separator: ",")
			var dict = [String: String]()

			listString.forEach {
				let item = $0.split(separator: ":")
				if let _key = item.first, let _value = item.last {
					dict[String(_key)] = String(_value)
				}
			}

			if let _latitudeString = dict["latitude"], let _longitudeString = dict["longitude"] {
				if let latitude = Double(_latitudeString), let longitude = Double(_longitudeString) {
					let time = dict["time"]
					
					return (latitude: latitude, longitude: longitude, time: time)
				}
			}
		}
		
		return nil
	}
	
	func compare(gps: String?, network: String?) -> Bool {
		guard let _gps = gps else { return false }
		guard let _network = network else { return false }
		
		guard let _gpsTime = TimeInterval(_gps), let _networkTime = TimeInterval(_network) else {
			return false
		}
		
		let gpsDate = _gpsTime / 1000
		let networkDate = _networkTime / 1000
		
		return gpsDate > networkDate - (60 * 60)
	}
}

