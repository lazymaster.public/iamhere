//
//  DefineShare.swift
//  MyLocatioin
//
//  Created by LazyMaster.iMac on 2021/08/25.
//

import UIKit


enum CallLocation {
	case main
	case shareUser
}

class DefineShare: NSObject {
	static let fcmAppKey = "AIzaSyABYoickCcAsdPHVy8wpTbvpiZF5XOyKz0"
	static var fcmFriendToken: String?
	static var fcmFriendName: String?
	
	static let KAKAO_PARAM_ACTION = "action"
	static let KAKAO_PARAM_TYPE = "type"
	static let KAKAO_PARAM_PUSHKEY = "pkey"
	static let KAKAO_PARAM_LATI = "lati"
	static let KAKAO_PARAM_LONGI = "longi"
	static let KAKAO_PARAM_TIME = "time"
	static let KAKAO_PARAM_PROVIDER = "pname"
	static let KAKAO_PARAM_IS_FAKEGPS = "isfake"
	static let KAKAO_PARAM_USER_NAME = "uname"
	static let KAKAO_PARAM_ANDROID = "android"
	static let KAKAO_PARAM_IOS = "ios"
	static let KAKAO_PARAM_ACTION_RECEIVE_LOCATION = "resLocation"
	static let KAKAO_PARAM_ACTION_SHARE_LOCATION_REQ = "shreReqLocation"
}

